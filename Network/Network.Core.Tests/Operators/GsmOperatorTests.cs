﻿namespace Two.Miasi.Network.Core.Tests.Operators
{
    using NSubstitute;

    using NUnit.Framework;

    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Networks;
    using Two.Miasi.Network.Core.Operators;

    public class GsmOperatorTests
    {
        public static GsmOperator DefaultGsmOperator
        {
            get
            {
                return new GsmOperator("Def");
            }
        }

        [TestFixture]
        public class RegisterDeviceTests
        {
            [Test]
            public void RegisterDevice_NewDevice_DeviceRegistered()
            {
                var gsmOperator = DefaultGsmOperator;
                var phoneNumber = Substitute.For<IPhoneNumber>();
                phoneNumber.Number = "34235424";
                var device = Substitute.For<MobilePhone>(phoneNumber, gsmOperator);

                gsmOperator.RegisterDevice(device);

                Assert.AreEqual(gsmOperator, device.CurrentOperator);
                Assert.AreEqual(device, gsmOperator.GetDevice(device.PhoneNumber));
            }
             
        }
    }
}