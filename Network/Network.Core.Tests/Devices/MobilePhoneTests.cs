﻿namespace Two.Miasi.Network.Core.Tests.Devices
{
    using NSubstitute;

    using NUnit.Framework;

    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Operators;

    public class MobilePhoneTests
    {
        public static IOperator DefaultOperator
        {
            get
            {
                var op = Substitute.For<IOperator>();
                op.Name = "Default";

                return op;
            }
        }

        public static MobilePhone DefaultMobilePhone
        {
            get
            {
                var number = Substitute.For<IPhoneNumber>();
                number.Number = "3423432423";

                return new MobilePhone(number, DefaultOperator);
            }
        }

        [TestFixture]
        public class CallTests
        {
            [Test]
            public void Call_DifferentOperator_Called()
            {
                var homeOperator = Substitute.For<IOperator>();
                var mobilePhone = new MobilePhone(new PhoneNumber("987654321"), homeOperator);
                var phoneNumber = new PhoneNumber("123456789");

                var otherOperator = Substitute.For<IOperator>();

                mobilePhone.ChangeOperator(otherOperator);
                mobilePhone.Call(phoneNumber);

                homeOperator.DidNotReceive().Connect(Arg.Any<IDevice>(), Arg.Any<IPhoneNumber>());
                otherOperator.Received().Connect(mobilePhone, phoneNumber);
            }

            [Test]
            public void Call_HomeOperator_Called()
            {
                var homeOperator = Substitute.For<IOperator>();
                var mobilePhone = new MobilePhone(new PhoneNumber("987654321"), homeOperator);
                var phoneNumber = new PhoneNumber("123456789");

                mobilePhone.Call(phoneNumber);

                homeOperator.Received(1).Connect(mobilePhone, phoneNumber);
            }
        }

        [TestFixture]
        public class ChangeOperatorTests
        {
            [Test]
            public void ChangeOperator_DifferentOperator_Changed()
            {
                MobilePhone mobilePhone = DefaultMobilePhone;
                var newOperator = Substitute.For<IOperator>();
                newOperator.Name = "New Operator";

                mobilePhone.ChangeOperator(newOperator);

                Assert.AreEqual(newOperator, mobilePhone.CurrentOperator);
            }
        }

        [TestFixture]
        public class CtorTests
        {
            [Test]
            public void Ctor_ElementsNotNull()
            {
                var phone = new MobilePhone(Substitute.For<IPhoneNumber>(), Substitute.For<IOperator>());

                Assert.NotNull(phone.Log);
                Assert.NotNull(phone.HomeOperator);
                Assert.NotNull(phone.CurrentOperator);
                Assert.NotNull(phone.PhoneNumber);
            }
        }

        [TestFixture]
        public class EndCallTests
        {
            [Test]
            public void EndCall_DifferentOperator_Called()
            {
                var homeOperator = Substitute.For<IOperator>();
                var mobilePhone = new MobilePhone(new PhoneNumber("987654321"), homeOperator);

                var otherOperator = Substitute.For<IOperator>();

                mobilePhone.ChangeOperator(otherOperator);
                mobilePhone.EndCall();

                homeOperator.DidNotReceive().Disconnect(Arg.Any<IDevice>());
                otherOperator.Received().Disconnect(mobilePhone);
            }

            [Test]
            public void EndCall_HomeOperator_Called()
            {
                var homeOperator = Substitute.For<IOperator>();
                var mobilePhone = new MobilePhone(new PhoneNumber("987654321"), homeOperator);

                mobilePhone.EndCall();

                homeOperator.Received(1).Disconnect(mobilePhone);
            }
        }
    }
}