﻿namespace Two.Miasi.Network.Core.Tests.Devices
{
    using NUnit.Framework;

    using Two.Miasi.Network.Core.Devices;

    public class PhoneNumberTests
    {
        public static PhoneNumber DefaultPhoneNumber
        {
            get
            {
                return new PhoneNumber("123456789");
            }
        }

        [TestFixture]
        public class EqualsTests
        {
            [Test]
            public void Equals_SameInstance_True()
            {
                var phoneNumber = DefaultPhoneNumber;
                
                Assert.True(phoneNumber.Equals(phoneNumber));
            }

            [Test]
            public void Equals_SameObject_True()
            {
                var phoneNumber = DefaultPhoneNumber;
                var otherPhoneNumber = DefaultPhoneNumber;

                Assert.True(phoneNumber.Equals(otherPhoneNumber));
                Assert.True(otherPhoneNumber.Equals(phoneNumber));
            }

            [Test]
            public void Equals_TwoDifferentObjects_False()
            {
                var phoneNumber = DefaultPhoneNumber;
                var otherPhoneNumber = new PhoneNumber("65432");

                Assert.False(phoneNumber.Equals(otherPhoneNumber));
                Assert.False(otherPhoneNumber.Equals(phoneNumber));
            }

            [Test]
            public void Equals_OneNull_False()
            {
                var phoneNumber = DefaultPhoneNumber;

                Assert.False(phoneNumber.Equals(null));
            }

            [Test]
            public void Equals_TwoTypes_False()
            {
                var phoneNumber = DefaultPhoneNumber;

                Assert.False(phoneNumber.Equals("5432"));
            }
        }
    }
}