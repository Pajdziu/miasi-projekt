﻿namespace Two.Miasi.Network.Core.Tests.Devices.Logs
{
    using System.Linq;

    using NSubstitute;

    using NUnit.Framework;

    using Two.Miasi.Network.Core.Devices.Logs;
    using Two.Miasi.Network.Core.Devices.Logs.Actions;

    public class LogTests
    {
        public static Log DefaultLog
        {
            get
            {
                return new Log();
            }
        }

        [TestFixture]
        public class AddActionTests
        {
            [Test]
            public void AddAction_EmptyList_Added()
            {
                var action = Substitute.For<IAction>();
                var log = DefaultLog;
                
                log.AddAction(action);

                Assert.AreEqual(1, log.Actions.Count());
                Assert.Contains(action, log.Actions.ToList());
            }

            [Test]
            public void AddAction_EmptyList_TwoElementsAdded()
            {
                var action = Substitute.For<IAction>();
                var log = DefaultLog;

                log.AddAction(action);
                log.AddAction(action);

                Assert.AreEqual(2, log.Actions.Count());
                Assert.Contains(action, log.Actions.ToList());
            }
        }

        [TestFixture]
        public class CtorTests
        {
            [Test]
            public void Ctor_ElementsNotNull()
            {
                var log = new Log();

                Assert.NotNull(log.ActionList);
                Assert.NotNull(log.Actions);
                Assert.IsEmpty(log.Actions);
            }
        }
    }
}