﻿namespace Two.Miasi.Network.Core.Tests.Clients
{
    using System.Collections.Generic;
    using System.Linq;

    using NSubstitute;

    using NUnit.Framework;

    using Two.Miasi.Network.Core.Customers;
    using Two.Miasi.Network.Core.Devices;

    public class PrivateCustomerTests
    {
        public static PrivateCustomer DefaultCustomer
        {
            get
            {
                return new PrivateCustomer("Jan", "Kowalski");
            }
        }

        public static IDevice DefaultDevice
        {
            get
            {
                var device = Substitute.For<IDevice>(new PhoneNumber("123"));
                return device;
            }
        }

        [TestFixture]
        public class AddDeviceTests
        {
            [Test]
            public void AddDevice_OneDevice_Added()
            {
                PrivateCustomer customer = DefaultCustomer;

                var device = Substitute.For<IDevice>(new PhoneNumber("123"));

                customer.AddDevice(device);

                Assert.AreEqual(1, customer.Devices.Count());
                Assert.Contains(device, customer.Devices.ToList());
            }

            [Test]
            public void AddDevice_TwoDeviceOneNumber_OneDeviceAdded()
            {
                PrivateCustomer customer = DefaultCustomer;

                var device = Substitute.For<IDevice>(new PhoneNumber("123"));
                var otherDevice = Substitute.For<IDevice>(new PhoneNumber("123"));

                customer.AddDevice(device);
                customer.AddDevice(otherDevice);

                Assert.AreEqual(1, customer.Devices.Count());
                Assert.Contains(otherDevice, customer.Devices.ToList());
            }
        }

        [TestFixture]
        public class CallTests
        {
            [Test]
            [ExpectedException(typeof(KeyNotFoundException))]
            public void Call_DeviceDoesNotExist_ExceptionThrown()
            {
                PrivateCustomer customer = DefaultCustomer;
                IDevice device = DefaultDevice;
                var target = new PhoneNumber("7654");

                customer.Call(device.PhoneNumber, target);

                device.DidNotReceive().Call(target);
            }

            [Test]
            public void Call_DeviceExists_Called()
            {
                PrivateCustomer customer = DefaultCustomer;
                IDevice device = DefaultDevice;
                var otherDevice = Substitute.For<IDevice>(new PhoneNumber("9492"));

                var target = new PhoneNumber("7654");

                customer.AddDevice(device);
                customer.AddDevice(otherDevice);
                customer.Call(device.PhoneNumber, target);

                device.Received(1).Call(target);
            }
        }

        [TestFixture]
        public class CtorTests
        {
            [Test]
            public void Ctor_DictionaryNotNull_True()
            {
                var customer = new PrivateCustomer(null, null);

                Assert.NotNull(customer.DevicesDictionary);
            }
        }

        [TestFixture]
        public class GetDevicesTests
        {
            [Test]
            public void GetDevice_NoDevice_EmptyList()
            {
                PrivateCustomer customer = DefaultCustomer;

                Assert.IsEmpty(customer.Devices);
            }

            [Test]
            public void GetDevice_TwoDevices_TwoElements()
            {
                PrivateCustomer customer = DefaultCustomer;

                var device = Substitute.For<IDevice>(new PhoneNumber("123"));
                var otherDevice = Substitute.For<IDevice>(new PhoneNumber("456"));

                customer.AddDevice(device);
                customer.AddDevice(otherDevice);

                Assert.AreEqual(2, customer.Devices.Count());
                Assert.Contains(device, customer.Devices.ToList());
                Assert.Contains(otherDevice, customer.Devices.ToList());
            }
        }

        [TestFixture]
        public class GetNameTests
        {
            [Test]
            public void GetName_FilledFirstNameLastName_FullName()
            {
                string firstName = "Jan";
                string lastName = "Kowalski";

                var customer = new PrivateCustomer(firstName, lastName);

                Assert.AreEqual(firstName, customer.FirstName);
                Assert.AreEqual(lastName, customer.LastName);
                Assert.That(customer.Name.Contains(firstName) && customer.Name.Contains(lastName));
            }

            [Test]
            public void GetName_NullFirstNameLastName_EmptyName()
            {
                var customer = new PrivateCustomer(null, null);
                Assert.NotNull(customer.FirstName);
                Assert.NotNull(customer.LastName);
            }
        }

        [TestFixture]
        public class RemoveDeviceTests
        {
            [Test]
            public void RemoveDevice_WasOneDevice_Empty()
            {
                PrivateCustomer customer = DefaultCustomer;

                IDevice device = DefaultDevice;
                customer.AddDevice(device);
                customer.RemoveDevice(device);

                Assert.IsEmpty(customer.Devices);
            }

            [Test]
            public void RemoveDevice_WasTwoDevices_OneDeviceLeft()
            {
                PrivateCustomer customer = DefaultCustomer;

                IDevice device = DefaultDevice;
                var otherDevice = Substitute.For<IDevice>(new PhoneNumber("543"));

                customer.AddDevice(device);
                customer.AddDevice(otherDevice);

                customer.RemoveDevice(device);

                Assert.AreEqual(1, customer.Devices.Count());
                Assert.Contains(otherDevice, customer.Devices.ToList());
            }
        }
    }
}