﻿namespace Two.Miasi.Network.Core.Customers
{
    using System.Collections.Generic;

    using Two.Miasi.Network.Core.Devices;

    public interface ICustomer
    {
        string Name { get; }

        IEnumerable<IDevice> Devices { get; }

        IDevice GetDevice(IPhoneNumber phoneNumber);

        void AddDevice(IDevice device);

        void RemoveDevice(IDevice device);

        void Call(IPhoneNumber sourceNumber, IPhoneNumber destinatioNumber);

        void EndCall(IPhoneNumber sourceNumber);
    }
}   