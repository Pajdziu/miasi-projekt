﻿namespace Two.Miasi.Network.Core.Customers
{
    using System.Collections.Generic;

    using NLog;

    using Two.Miasi.Network.Core.Devices;

    public class PrivateCustomer : ICustomer
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public PrivateCustomer(string firstName, string lastName)
            : this()
        {
            this.FirstName = firstName ?? string.Empty;
            this.LastName = lastName ?? string.Empty;

            Logger.Info("Created {0}", this);
        }

        private PrivateCustomer()
        {
            this.DevicesDictionary = new Dictionary<IPhoneNumber, IDevice>();
        }

        public IDictionary<IPhoneNumber, IDevice> DevicesDictionary { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Name
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        public IEnumerable<IDevice> Devices
        {
            get
            {
                return this.DevicesDictionary.Values;
            }
        }

        public IDevice GetDevice(IPhoneNumber phoneNumber)
        {
            return this.DevicesDictionary[phoneNumber];
        }

        public void AddDevice(IDevice device)
        {
            this.DevicesDictionary[device.PhoneNumber] = device;
            Logger.Info("{0} added {1}", this, device);
        }

        public void RemoveDevice(IDevice device)
        {
            this.DevicesDictionary.Remove(device.PhoneNumber);
            Logger.Info("{0} removed {1}", this, device);
        }

        public void Call(IPhoneNumber sourceNumber, IPhoneNumber destinatioNumber)
        {
            var device = this.DevicesDictionary[sourceNumber];
            device.Call(destinatioNumber);
        }

        public void EndCall(IPhoneNumber sourceNumber)
        {
            var device = this.DevicesDictionary[sourceNumber];
            device.EndCall();
        }

        public override string ToString()
        {
            return string.Format("PrivateCustomer <{0}>", this.Name);
        }
    }
}