﻿namespace Two.Miasi.Network.Core.Networks
{
    using System;
    using System.Collections.Generic;

    using NLog;

    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Operators;
    using Two.Miasi.Network.Core.Operators.Rates;

    public class GsmNetwork : INetwork
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDictionary<IPhoneNumber, IOperator> operatorForNumberDictionary;

        private readonly IList<IOperator> operators; 

        public GsmNetwork(string name)
        {
            this.Name = name;
            this.operatorForNumberDictionary = new Dictionary<IPhoneNumber, IOperator>();
            this.operators = new List<IOperator>();

            Logger.Info("Created {0}", this);
        }

        public string Name { get; set; }

        public IDictionary<IOperator, IDictionary<IOperator, IRate>> Rates { get; set; }

        public double GetCallRate(IOperator sourceOperator, IOperator destinationOperator)
        {
            return this.Rates[sourceOperator][destinationOperator].CallRate;
        }

        public double GetRoamingCallRate(
            IOperator sourceOperator, 
            IOperator destinationOperator, 
            IOperator homeOperator)
        {
            return this.Rates[sourceOperator][destinationOperator].RoamingCallRate;
        }

        public IOperator GetCurrentOperatorForNumber(IPhoneNumber number)
        {
            return this.operatorForNumberDictionary[number];
        }

        public void RegisterOperator(IOperator newOperator)
        {
            this.operators.Add(newOperator);
            newOperator.Network = this;

            Logger.Info("{0} registered {1}", this, newOperator);
        }

        public void RegisterDevice(IPhoneNumber phoneNumber, IOperator currentOperator)
        {
            this.operatorForNumberDictionary[phoneNumber] = currentOperator;
        
            Logger.Info("{0} registered {1} to {2}", this, phoneNumber, currentOperator);
        }

        public override string ToString()
        {
            return string.Format("GsmNetwork <{0}>", this.Name);
        }
    }
}