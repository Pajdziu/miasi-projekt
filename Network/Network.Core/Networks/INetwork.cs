﻿namespace Two.Miasi.Network.Core.Networks
{
    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Operators;

    public interface INetwork
    {
        double GetCallRate(IOperator sourceOperator, IOperator destinationOperator);

        double GetRoamingCallRate(IOperator sourceOperator, IOperator destinationOperator, IOperator homeOperator);

        IOperator GetCurrentOperatorForNumber(IPhoneNumber number);

        void RegisterOperator(IOperator newOperator);

        void RegisterDevice(IPhoneNumber phoneNumber, IOperator currentOperator);
    }
}