﻿namespace Two.Miasi.Network.Core.Operators
{
    using System;
    using System.Collections.Generic;

    using NLog;

    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Devices.Logs.Actions;
    using Two.Miasi.Network.Core.Networks;
    using Two.Miasi.Network.Core.Operators.Rates;

    public class GsmOperator : IOperator
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDictionary<IPhoneNumber, IDevice> devices;

        private readonly IDictionary<IDevice, IAction> currentActions; 

        public GsmOperator(string name)
        {
            this.Name = name;
            this.devices = new Dictionary<IPhoneNumber, IDevice>();
            this.currentActions = new Dictionary<IDevice, IAction>();

            Logger.Info("Created {0}", this);
        }

        public string Name { get; set; }

        public IDictionary<IOperator, IRate> Rates { get; private set; }

        public INetwork Network { get; set; }

        public void RegisterDevice(IDevice device)
        {
            this.devices[device.PhoneNumber] = device;
            Logger.Info("{0} registered {1}", this, device);

            this.Network.RegisterDevice(device.PhoneNumber, this);

        }

        public void Connect(IDevice source, IPhoneNumber destination)
        {
            IOperator destOperator = this.Network.GetCurrentOperatorForNumber(destination);
            IDevice destDevice = destOperator.GetDevice(destination);
            destDevice.StartCall(source);

            var action = new PhoneCall(source, destDevice);
            this.currentActions[source] = action;
            this.currentActions[destDevice] = action;

            Logger.Info("{0} connected {1} to {2}", this, source, destDevice);
        }

        public void Disconnect(IDevice device)
        {
            var action = this.currentActions[device];

            action.Destination.EndCall();
            action.Duration = DateTime.Now.ToLocalTime() - action.Date;

            action.Source.Log.AddAction(action);
            action.Destination.Log.AddAction(action);

            this.currentActions.Remove(action.Source);
            this.currentActions.Remove(action.Destination);

            Logger.Info("{0} disconnected {1} and {2}", action.Source, action);
        }

        public IDevice GetDevice(IPhoneNumber phoneNumber)
        {
            return this.devices[phoneNumber];
        }

        public override string ToString()
        {
            return string.Format("GsmOperator <{0}>", this.Name);
        }
    }
}