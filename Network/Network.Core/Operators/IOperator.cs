﻿namespace Two.Miasi.Network.Core.Operators
{
    using System.Collections.Generic;

    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Networks;
    using Two.Miasi.Network.Core.Operators.Rates;

    public interface IOperator
    {
        string Name { get; set; }

        IDictionary<IOperator, IRate> Rates { get; }

        INetwork Network { get; set; }

        void RegisterDevice(IDevice device);

        void Connect(IDevice source, IPhoneNumber destination);

        void Disconnect(IDevice source);

        IDevice GetDevice(IPhoneNumber phoneNumber);
    }
}