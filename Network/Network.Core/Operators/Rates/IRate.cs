﻿namespace Two.Miasi.Network.Core.Operators.Rates
{
    public interface IRate
    {
        IOperator SourceOperator { get; }

        IOperator DestinationOperator { get; }

        double CallRate { get; }

        double RoamingCallRate { get; }

        double TextMessageRate { get; }

        double MultimediaMessageRate { get; }
    }
}