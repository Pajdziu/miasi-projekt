﻿namespace Two.Miasi.Network.Core.Operators.Rates
{
    public class GsmRate : IRate
    {
        public IOperator SourceOperator { get; private set; }

        public IOperator DestinationOperator { get; private set; }

        public double CallRate { get; private set; }

        public double RoamingCallRate { get; private set; }

        public double TextMessageRate { get; private set; }

        public double MultimediaMessageRate { get; private set; }
    }
}