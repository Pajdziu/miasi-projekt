﻿namespace Two.Miasi.Network.Core.Devices.Logs
{
    using System;
    using System.Collections.Generic;

    using Two.Miasi.Network.Core.Devices.Logs.Actions;

    public interface ILog
    {
        IEnumerable<IAction> Actions { get; }

        void AddAction(IAction action);
    }
}