﻿namespace Two.Miasi.Network.Core.Devices.Logs
{
    using System;
    using System.Collections.Generic;

    using NLog;

    using Two.Miasi.Network.Core.Devices.Logs.Actions;

    public class Log : ILog
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public Log()
        {
            this.ActionList = new List<IAction>();
        }

        public IList<IAction> ActionList { get; set; }

        public IEnumerable<IAction> Actions
        {
            get
            {
                return this.ActionList;
            }
        }

        public void AddAction(IAction action)
        {
            this.ActionList.Add(action);

            Logger.Info("Added {0} to {1}", action, this);
        }
    }
}