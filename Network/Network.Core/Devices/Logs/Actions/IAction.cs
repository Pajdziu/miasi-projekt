﻿namespace Two.Miasi.Network.Core.Devices.Logs.Actions
{
    using System;

    public interface IAction
    {
        DateTime Date { get; set; }

        TimeSpan Duration { get; set; }

        double Cost { get; set; }

        IDevice Source { get; set; }

        IDevice Destination { get; set; }
    }
}