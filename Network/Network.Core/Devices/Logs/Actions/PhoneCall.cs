﻿namespace Two.Miasi.Network.Core.Devices.Logs.Actions
{
    using System;

    public class PhoneCall : IAction
    {
        public PhoneCall(IDevice source, IDevice destination)
        {
            this.Source = source;
            this.Destination = destination;
            this.Date = DateTime.UtcNow.ToLocalTime();
        }

        public DateTime Date { get; set; }

        public TimeSpan Duration { get; set; }

        public double Cost { get; set; }

        public IDevice Source { get; set; }

        public IDevice Destination { get; set; }
    }
}