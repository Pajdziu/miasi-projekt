﻿namespace Two.Miasi.Network.Core.Devices
{
    public class PhoneNumber : IPhoneNumber
    {
        public PhoneNumber(string number)
        {
            this.Number = number;
        }

        public string Number { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return string.Equals(this.Number, ((PhoneNumber)obj).Number);
        }

        public override int GetHashCode()
        {
            return this.Number != null ? this.Number.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return string.Format("PhoneNumber <{0}>", this.Number);
        }
    }
}