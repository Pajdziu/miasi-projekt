﻿namespace Two.Miasi.Network.Core.Devices
{
    using System;

    using NLog;

    using Two.Miasi.Network.Core.Devices.Logs;
    using Two.Miasi.Network.Core.Operators;

    public class MobilePhone : IDevice
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MobilePhone(IPhoneNumber phoneNumber, IOperator homeOperator)
        {

            this.PhoneNumber = phoneNumber;
            this.HomeOperator = homeOperator;
            this.CurrentOperator = homeOperator;
            this.Log = new Log();

            Logger.Info("Created {0}", this);

            this.HomeOperator.RegisterDevice(this);
        }

        public IPhoneNumber PhoneNumber { get; private set; }

        public IOperator CurrentOperator { get; private set; }

        public IOperator HomeOperator { get; private set; }

        public ILog Log { get; private set; }

        public void ChangeOperator(IOperator newOperator)
        {
            newOperator.RegisterDevice(this);
            this.CurrentOperator = newOperator;

            Logger.Info("{0} changed operator to {1}", this, this.CurrentOperator);
        }

        public void Call(IPhoneNumber phoneNumber)
        {
            Logger.Info("{0} called {1}", this, phoneNumber);

            this.CurrentOperator.Connect(this, phoneNumber);
        }

        public void StartCall(IDevice device)
        {
            Logger.Info("{0} answered from {1}", this, device);
        }

        public void EndCall()
        {
            this.CurrentOperator.Disconnect(this);

            Logger.Info("{0} ended call", this);
        }

        public override string ToString()
        {
            return string.Format("MobilePhone <{0}>", this.PhoneNumber.Number);
        }
    }
}