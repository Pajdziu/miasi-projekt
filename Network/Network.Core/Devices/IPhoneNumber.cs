﻿namespace Two.Miasi.Network.Core.Devices
{
    public interface IPhoneNumber
    {
        string Number { get; set; }
    }
}