﻿namespace Two.Miasi.Network.Core.Devices
{
    using Two.Miasi.Network.Core.Devices.Logs;
    using Two.Miasi.Network.Core.Operators;

    public interface IDevice
    {
        IPhoneNumber PhoneNumber { get; }

        IOperator CurrentOperator { get; }

        IOperator HomeOperator { get; }

        ILog Log { get; }

        void ChangeOperator(IOperator newOperator);

        void Call(IPhoneNumber phoneNumber);

        void StartCall(IDevice device);

        void EndCall();
    }
}