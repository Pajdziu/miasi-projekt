﻿namespace Two.Miasi.Network.Console
{
    using NLog;

    using Two.Miasi.Network.Core.Customers;
    using Two.Miasi.Network.Core.Devices;
    using Two.Miasi.Network.Core.Networks;
    using Two.Miasi.Network.Core.Operators;

    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            Logger.Info("App start");

            var network = new GsmNetwork("PL");

            var play = new GsmOperator("Play");
            var orange = new GsmOperator("Orange");
            var tmobile = new GsmOperator("T-Mobile");
            var plus = new GsmOperator("Plus");

            network.RegisterOperator(play);
            network.RegisterOperator(orange);
            network.RegisterOperator(tmobile);
            network.RegisterOperator(plus);

            var user1 = new PrivateCustomer("Jan", "Kowalski");
            var user2 = new PrivateCustomer("Stefan", "Nowak");

            var dev1 = new MobilePhone(new PhoneNumber("123"), play);
            var dev2 = new MobilePhone(new PhoneNumber("456"), orange);

            user1.AddDevice(dev1);
            user2.AddDevice(dev2);

            user1.Call(new PhoneNumber("123"), new PhoneNumber("456"));
        }
    }
}